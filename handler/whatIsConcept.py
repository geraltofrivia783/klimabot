def handle(data):
  import wikipedia
  import config.config as config
  wikipedia.set_lang(config.wikipediaLanguage)

  # we only have one slot that can be filled, which is a concept
  concept = data['slots'][0]['rawValue'];
  summary = "Ich kenne '" + concept + "' leider noch nicht."
  try:
    summary = wikipedia.summary(concept, sentences=config.summarySentences)
  except wikipedia.exceptions.DisambiguationError as e:
    summary = wikipedia.summary(e.options[0], sentences=config.summarySentences)
  except wikipedia.exceptions.PageError as pe:
    print("Could not find Wikipedia page for " + concept);

  data['answer'] = summary;
  return data;