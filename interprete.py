import sys
import glob
from importlib import import_module
from pprint import pprint as pp

from snips_nlu import SnipsNLUEngine

def parseText(text):
    models = glob.glob('data/models/*')
    if not models:
        print('No pretrained models found. Please run train.py first.')
        sys.exit()
    latest_model = sorted(models)[-1]
    print("Loading following model: " + latest_model)
    engine = SnipsNLUEngine.from_path(latest_model)
    return engine.parse(text)

def handleIntent(data):
    return_value = data
    try:
        intent = data['intent']['intentName']
        handler = import_module('handler.{intent}'.format(intent=intent)).handle
        print("Loaded handler for ", intent)
    except:
        print("Could not load handler for ", intent)
        handler = lambda x: x
    return handler(data)

def main():
    argument_length = len(sys.argv)
    if argument_length < 2:
        print('Text to parse is missing.')
        sys.exit()
    elif argument_length > 2:
        print('To many arguments.')
        sys.exit()
    else:
        data = parseText(sys.argv[-1])
        pp(handleIntent(data))
        sys.exit()


if __name__ == '__main__':
    main()
