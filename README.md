# Klimabot or climate bot
This project is a first attempt to build a comprehensive knowledge base about climate facts that is easily accessible and extensible. We use state of the art Natural Language Understanding tools to match natural language questions to our internal data structure and access public APIs to find suitable answers.

## Table of Contents

- [Klimabot or climate bot](#klimabot-or-climate-bot)
  - [Table of Contents](#table-of-contents)
  - [Contribution guide](#contribution-guide)
    - [Content](#content)
    - [Code](#code)
    - [Contributors](#contributors)
  - [Possible datasets and APIs](#possible-datasets-and-apis)
    - [Interesting APIs and API clients](#interesting-apis-and-api-clients)
    - [Currently looked at data](#currently-looked-at-data)
    - [Possible future datasets](#possible-future-datasets)
  - [Questions and answers](#questions-and-answers)
    - [Climate bot answer schema](#climate-bot-answer-schema)
      - [Supported questions](#supported-questions)
      - [Not yet supported questions](#not-yet-supported-questions)
        - [Temperatur](#temperatur)
        - [Niederschlag](#niederschlag)
        - [Wind](#wind)
        - [Solar](#solar)
        - [Kohle](#kohle)
        - [CO2](#co2)
  - [Development](#development)
    - [Installation](#installation)
    - [Training des Models](#training-des-models)
    - [Testen des Ergebnisses](#testen-des-ergebnisses)
  - [License](#license)

## Contribution guide
There are two main versions of contributions that you can make. Both types are equally important. First we constantly try to improve the answer quality and extend the covered thematic regions. Second we want to extend the actual source code, e.g. by providing text to speech question input, text to speech answer outputs, more fancy answer represenation, Telegram integration or a mobile app.

For both versions you would need to create a `Merge Request` in Gitlab. In order to do that, please create a Gitlab account and fork this project. You can then change thl then code in your repository and once you have that you can create a merge request to this repository again. We will then review and discuss your modifications and merge it once everything is resolved into our project. In case you don't know what a merge request is, and also don't want to find out, please contact @gerbsen. Getting in touch before doing anything might be a good starting point. :)

### Content
Content is the corner stone of this bot.

### Code

### Contributors
- https://gitlab.com/deb_vortex
- https://gitlab.com/gerbsen

## Possible datasets and APIs

### Interesting APIs and API clients
**https://github.com/hiveeyes/dwdweather2**: This is a python API client which is able to query the data by Deutscher Wetter Dienst (DWD).

### Currently looked at data
- **http://openclimatedata.net/:** Pretty impressive list of all sorts of climate related data, ranging from *Global Carbon Budge*t over *EDGAR Global Emissions* Dataset
(Emissions for the three main greenhouse gases per sector and country) to *Entry Into Force of the Paris Climate Agreement*
- **https://cds.climate.copernicus.eu/api-how-to**: Dive into this wealth of information about the Earth's past, present and future climate. It is freely available and functions  as a one-stop shop to explore climate data. Register for free to obtain access to the CDS and its Toolbox.
- **http://opendata.dwd.de/**: Zurzeit stehen viele Geodaten wie Modellvorhersagen, Radardaten, aktuelle Mess- und Beobachtungsdaten sowie eine große Zahl von Klimadaten auf dem Open Data Server https://opendata.dwd.de zur Verfügung. Die Klimadaten werden unter https://opendata.dwd.de/climate_environment/CDC/ bereitgestellt.

### Possible future datasets
The following list is just a list of unreviewed links from a set of  internet searches:

- [Kraftwerksliste der Bundesnetzagentur](https://www.bundesnetzagentur.de/DE/Sachgebiete/ElektrizitaetundGas/Unternehmen_Institutionen/Versorgungssicherheit/Erzeugungskapazitaeten/Kraftwerksliste/kraftwerksliste-node.html)
- [Erzeugung, Abgabe und Verbrauch von Strom und Gas, Wärmeerzeugung, Energieverbrauch in Industrie und Privathaushalten](https://www.statistikportal.de/de/energie)
- http://www.ipcc-data.org/
- http://www.climatus.com/index.php
- http://www.ncdc.noaa.gov/sotc/global/2012/10
- http://worldclim.org/version2
- http://www.cru.uea.ac.uk/data
- https://www.globalclimatemonitor.org/
- https://darksky.net/dev/docs

## Questions and answers
### Climate bot answer schema

**answer:** Is HTML text that is computed by the handle function of a given intent-handler.

**additionalInformation:** Is a list of additional information. Sometimes it might be useful to display additional data or images. E.g for long running trends it might be interesting to see temperatur, CO<sub>2</sub> concentration or gas prices over time.

-- **name:** a descriptive name (HTML) of the additional information, e.g. 'Temperature anomalies'

-- **description:** a description (HTML) of the additional information, might be a longer text with links

-- **images:** An array of images that belong to this additional information

---- **name:** The name for the image (HTML)

---- **description:** A description for the image (HTML)

---- **url:** A relative URL for the image, if the image is hosted locally, a absolute URL if the image is remote.

---- **copyright:** Copyright information (HTML)

---- **license:** license information (HTML)

-- **data:** an object which contains arbitrary data, might be used to send plain values to the client to display native tables instead of plain old images

---- **key:** the key of the dataset

---- **value:** the values of the dataset, might be arbitrary for now

```
{
  "answer": "Der Verkehrsverbund Vogtland (VVV) ist ein Verkehrsverbund im Südwesten Sachsens. Das Verbundgebiet umfasst die Fläche des  Vogtlandkreises.",
  "additionalInformation" : [
      {
          "name":
          "description":
          "images": [
              {
                  "name":
                  "url":
                  "description":
              }
          ]
          "data": {
              "key": "value"
          }
      }
  ]
  "input": "was versteht man unter zvv",
  "intent": {
    "intentName": "whatIsConcept",
    "probability": 0.681833022572168
  },
  "slots": [
    {
      "entity": "concept",
      "range": {
        "end": 26,
        "start": 23
      },
      "rawValue": "zvv",
      "slotName": "concept",
      "value": {
        "kind": "Custom",
        "value": "zvv"
      }
    }
  ]
}
```

#### Supported questions

1. "What is **CONCEPT?**": This question type is for answering definition question, e.g. *'What is the climate crisis?'*. To answer these questions we are using the [Wikipedia API summary function](https://wikipedia.readthedocs.io/en/latest/quickstart.html#quickstart).
   1. Slots: `concept`
   2. Intent File: `data/intents/whatIsConcept.yaml`
   3. Entity File: `data/concept/concept.yaml`
2. "How much CO2 does **COUNTRY** create"?:
   1. Source Data:
      1. https://en.wikipedia.org/wiki/List_of_countries_by_carbon_dioxide_emissions


#### Not yet supported questions
##### Temperatur
- Wie hoch war die Tagesdurchschnittstemperatur in **$PLACE** am **$DATE**?
- Wie hoch war die Monatsdurchschnittstemperatur in **$PLACE** im **$MONAT**?
- Wie hoch war die Jahresdurchschnittstemperatur in **$PLACE** im Jahr **$JAHR**?
- Wie groß ist die Abweichung der Durchschnittstemperatur in **$PLACE** im Vergleich zu den letzten 30 Jahren?

##### Niederschlag

##### Wind
- Wieviele Windräder stehen in **$ORT**?
- Wieviel Leistung liefert ein durchschnittliches Windrad?
- Wieviele Windräder stehen in **$ORT** im Jahr **$JAHR**?
- Wieviele Windräder wurden im Jahr **$JAHR** in **$ORT** gebaut?

##### Solar
- Wieviel Energie liefert ein Quadratmeter Solarpanel?
- Wieviele Solarparks gibt es in **$ORT**?

##### Kohle
- Wieviele Kohlekraftwerke gibt es?
- Wieviele Kohlekraftwerke gibt es in **$ORT**?

##### CO2
- Wie hoch ist der CO2 Ausstoß von Menschen in **$ORT**?
- Wie hoch war der CO2 Ausstoß von Menschen in **$ORT** im **$JAHR** ?
- Wie hoch ist der CO2 Ausstoß vom Kraftwerk **$KRAFTWERK**?

## Development

### Installation

1. Install `pyenv` according to [this](https://github.com/pyenv/pyenv#installation) tutorial.
   1. For OS X it's just: `brew update && brew install pyenv`
2. Now run the following commands
```bash
// this will let you switch between python versions seamlessly
$ pyenv install 3.5.2
// we use 3.5.2 python for now
$ pyenv local 3.5.2
$ python3 -m venv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
$ snips-nlu download de
```

### Training des Models

Zum trainieren und hinzufügen von neuen intents, muss die `yaml` Datei nach `data/intents` gelegt werden. Der Name der Datei muss dem Namen des intents entsprechen. Die Trainingsdaten für ein intent über `solar` muss also `data/intents/solar.yaml` heißen. Selbes gilt für entities.

Wenn nach dem Training nun der intent `solar` erkannt wird, werden die erkannten Informationen an die Funktion `handle` aus der `handler/solar.py` weiter gegeben. Das Ergebnis dieser Funktion wird anschließend zurück gegeben.

Zum einsammeln der Trainingsdaten und trainieren wird einfach die `train.py` ausgeführt. Dazu bitte sicher gehen, dass das virtual environment aktiviert ist. Dies erkennt man daran, das `(venv)` vor dem console prompt steht. Wenn dies nicht der Fall ist, kann das venv mittels `$ source venv/bin/activate` vom root Verzeichnis des Projektes aus gestartet werden.

### Testen des Ergebnisses

Anschließend kann das Trainingsergebnis getestet werden mit Hilfe von
`python interprete.py "<TEXT HERE>"`. Wenn ein `intent` erkannt wurde, wird versucht die `handle` Funktion aus der `handlers.<intent name>` zu importieren. Wenn dies erfolgreich ist, werden die extrahierten Informationen an die `handle` Funktion übergeben. Alternativ wird das Ergebnis von SNIPS ausgegeben.

## License

[MIT](https://opensource.org/licenses/MIT) © 2019 Daniel Gerber

See license in [LICENSE.txt](./LICENSE.txt)